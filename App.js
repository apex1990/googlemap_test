import React from 'react';
import { StyleSheet, Text, View, TouchableOpacity, Image, Animated } from 'react-native';
import MapView, { Marker } from 'react-native-maps';

const locations = require('./location.json')

const App = () => {

  state= {
    latitude: null,
    longitude: null,
    locations: locations
  }
  const onMarkerPress = (e) => {
    const markerId = e._targetInst.return.key;
    let x = (markerId * 330) + (markerId * 20);

    _scrollView.current.scrollTo({
      x: x, y: 0, animated: true
    });
  }

  const getCoordinates = (region) => {
    console.log('region : ', region)
  }

  const renderMarkers = () => {
    return (
      <View>
        {
          locations.map((location, index) => {
            return (
              <Marker
                key={index}
                coordinate={{latitude: location.coords.latitude, longitude: location.coords.longitude}}
                onPress={(e) => onMarkerPress(e)}
              >
                <Animated.View style={[styles.recMarker, { backgroundColor: backColors[index] }]}>
                  <Text style={{color : backColors[index] === 'gray' ? 'white' : 'black'}}>$ {location.price}</Text>
              </Animated.View>
              </Marker>
            )
          })
        }
      </View>
    )
  }
    const _map = React.useRef(null);
    const _scrollView = React.useRef(null);
    const [backColors, setBackColor] = React.useState(['gray','white','white','white','white','white','white','white','white','white','white','white','white','white','white']);
    const initBackColors = ['white','white','white', 'white','white','white','white','white','white','white','white','white','white','white','white']

    React.useEffect(() => {
      mapAnimations.addListener(({ value }) => {
        let index = Math.floor(value / 330 + 0.3);
        if (index >= locations.length) {
          index = locations.length - 1;
        }
        if (index <= 0) {
          index = 0;
        }

        let newColors = [...initBackColors];
          newColors[mapIndex] = 'gray'
            setBackColor(newColors)
          if( mapIndex !== index ) {
            mapIndex = index;
            const { coords } = locations[index];
            _map.current.animateToRegion (
              {
                ...coords,
                latitudeDelta: 0.0486234,
                longitudeDelta: 0.0486234
              },
              350
            )
          }

        // clearTimeout(regionTimeout);

        // const regionTimeout = setTimeout(() => {
        //   let newColors = [...initBackColors];
        //   newColors[mapIndex] = 'gray'
        //     setBackColor(newColors)
        //   if( mapIndex !== index ) {
        //     mapIndex = index;
        //     const { coords } = locations[index];
        //     _map.current.animateToRegion (
        //       {
        //         ...coords,
        //         latitudeDelta: 0.0486234,
        //         longitudeDelta: 0.0486234
        //       },
        //       350
        //     )
        //   }
        // }, 10);
      })
    })

    let mapIndex = 0;
    let mapAnimations = new Animated.Value(0)
      return (
        <View style={{flex: 1}}>
          <MapView
            ref={_map}
            showsUserLocation
            style={{flex: 1}}
            
            initialRegion={{
              latitude: 40.1014929,
              longitude: 124.3550536,
              latitudeDelta: 0.0922,
              longitudeDelta: 0.0421
            }}
          >
            {renderMarkers()}
          </MapView>
          <Animated.ScrollView
            ref={_scrollView}
            horizontal
            scrollEventThrottle={1}
            showsHorizontalScrollIndicator={false}
            pagingEnabled
            snapToInterval={350}
            snapToAlignment='center'
            style={styles.cardView}
            onScroll={Animated.event(
              [
                {
                  nativeEvent: {
                    contentOffset: {
                      x: mapAnimations
                    }
                  }
                }
              ],
              {useNativeDriver: true}
            )}
          >
            {
              locations.map((item, index) => {
                return (
                  <TouchableOpacity key={index} style={styles.card}>
                    <Image source={{
                      uri: item.image_url
                    }}
                      style={{height: 100, width: 100, resizeMode: 'stretch', borderTopLeftRadius: 20, borderBottomLeftRadius: 20}}
                    />
                    <View style={{justifyContent: 'center', alignItems: 'left', width: 230, paddingLeft: 30}}>
                      <Text>{item.price}</Text>
                    </View>
                  </TouchableOpacity>
                )
              })
            }
          </Animated.ScrollView>
        </View>
      );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  recMarker: {
    height: 30,
    width: 40,
    backgroundColor: 'white',
    borderRadius: 15,
    justifyContent: 'center',
    alignItems: 'center',
    shadowColor: "#939393",
    shadowOffset: {
        width: 1,
        height: 1,
    },
    shadowOpacity: 0.3,
    shadowRadius: 2,
    elevation: 1,
  },
  cardView: {
    display: 'flex',
    position: 'absolute',
    bottom: 10,
    paddingLeft: 10
  },
  card: {
    height: 100,
    width: 330,
    flexDirection: 'row',
    backgroundColor: 'white',
    justifyContent: 'space-between',
    alignItems: 'center',
    shadowColor: "#939393",
    shadowOffset: {
        width: 1,
        height: 1,
    },
    shadowOpacity: 0.3,
    shadowRadius: 2,
    elevation: 1,
    marginRight: 20,
    borderRadius: 20
  }
});

export default  App